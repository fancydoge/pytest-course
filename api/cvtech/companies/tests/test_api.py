from django.urls import reverse
from companies.models import Company
import json
import pytest

#глобальный декоратор, чтобы не писать перед каждой ф-цией(тестом) @pytest.mark.django_db
pytestmark = pytest.mark.django_db
companies_url = reverse('companies-list')

#теперь это ф-ция без класса, так что нужно вручную добавить db декоратор, а так же заюзать стандартный fixture pytest - client
def test_zero_companies_should_return_empty_list(client)->None:
    response = client.get(companies_url)
    assert response.status_code == 200
    assert json.loads(response.content) == []

def test_one_company_exists_should_succeed(client)->None:
    test_company = Company.objects.create(name='Psina Company')
    response = client.get(companies_url)
    response_content = json.loads(response.content)[0]
    assert response.status_code == 200
    assert response_content.get('name') == test_company.name


# ---------------Test Post Companies---------------
def test_create_company_without_args_should_fail(client) -> None:
    response = client.post(companies_url)
    assert response.status_code == 400
    assert json.loads(response.content) == {'name': ['This field is required.']}

def test_create_existing_company_should_fail(client) -> None:
    Company.objects.create(name='Kek Company')
    response = client.post(companies_url, {'name': 'Kek Company'})
    assert response.status_code == 400
    assert json.loads(response.content) == {'name': ['company with this name already exists.']}

def test_create_company_with_only_name_other_fields_sould_be_default(client) -> None:
    response = client.post(companies_url, {'name': 'test company name'})
    assert response.status_code == 201
    response_content = json.loads(response.content)
    assert response_content.get('name') == 'test company name'
    assert response_content.get('status') == 'Hiring'
    assert response_content.get('application_link') == ''
    assert response_content.get('notes') == ''

def test_create_company_with_layoffs_should_succeed(client) -> None:
    response = client.post(companies_url, {'name': 'Kek Company', 'status': 'Layoffs'})
    assert response.status_code == 201
    response_content = json.loads(response.content)
    assert response_content.get('name') == 'Kek Company'
    assert response_content.get('status') == 'Layoffs'

def test_create_company_with_wrong_status_should_fail(client) -> None:
    response = client.post(companies_url, {'name': 'Kek Company', 'status': 'wrong status'})
    assert response.status_code == 400
    assert 'is not a valid choice.' in str(response.content)
    print(response.content)

#тут client не нужен
@pytest.mark.xfail
def test_should_be_ok_if_fail() -> None:
    assert 1 == 2


@pytest.mark.skip
def test_should_be_skipped() -> None:
    assert 1 == 2