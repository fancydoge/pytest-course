from unittest import TestCase
from django.test import Client
from django.urls import reverse
from companies.models import Company
import json

import pytest
#unittest - стандартный тест фреймворк, включенный в python
#все что можно сделать с помощью unittest, можно сделать и с помощью PyTest, но гораздо быстрее и проще и чище


#класс, который будут наследовать все тест классы, чтобы не прописывать setUp and tearDown заново
@pytest.mark.django_db
class BasicCompanyAPiTestCase(TestCase):
    #так клиент джанго не нужно прописывать для каждого теста отдельно
    #setUp в unittest обозначает все что будет запущено перед тестом, а tearDown - после
    def setUp(self) -> None:
        self.client = Client()
        self.companies_url = reverse('companies-list')

    def tearDown(self) -> None:
        pass

#Самый простой кейс - есть ли компании в датабазе вообще?
class TestGetCompanies(BasicCompanyAPiTestCase):
    #префикс test_, чтобы pytest мог его видеть
    #zero_companies - чать, которая говорит о state(случае,видимо), который рассматривается
    #ну и остальная часть - ожидаемый результат
    # -> None - это просто примечание о том, что должна возвращать ф-ция, а так она может вернуть что угодно
    def test_zero_companies_should_return_empty_list(self)->None:
        #берем тестовый клиент django^, который работает как тот же Postman
        #client = Client()
        #url, куда будут посылаться запросы, но хардкодить их - плохая практика
        #companies_url = "http://127.0.0.1:8000/companies/"
        #companies_url = reverse('companies-list')
        response = self.client.get(self.companies_url)
        #неудобный синтаксис unittest/django тестов
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), [])

    def test_one_company_exists_should_succeed(self)->None:
        test_company = Company.objects.create(name='Psina Company')
        #companies_url = reverse('companies-list')
        response = self.client.get(self.companies_url)
        #т.к response.content - лист, нужно взять первый элемент, т.к. тести наличие одной компании
        response_content = json.loads(response.content)[0]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_content.get('name'), test_company.name)



class TestPostCompanies(BasicCompanyAPiTestCase):
    def test_create_company_without_args_should_fail(self) -> None:
        #второй аргумент в post - пейлоад, который тут, очевидно, должен быть пустым
        response = self.client.post(self.companies_url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content), {'name': ['This field is required.']})

    def test_create_existing_company_should_fail(self) -> None:
        Company.objects.create(name='Kek Company')
        response = self.client.post(self.companies_url, {'name': 'Kek Company'})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content), {'name': ['company with this name already exists.']})


    def test_create_company_with_only_name_other_fields_sould_be_default(self) -> None:
        response = self.client.post(self.companies_url, {'name': 'test company name'})
        self.assertEqual(response.status_code, 201)
        response_content = json.loads(response.content)
        self.assertEqual(response_content.get('name'), 'test company name')
        self.assertEqual(response_content.get('status'), 'Hiring')
        self.assertEqual(response_content.get('application_link'), '')
        self.assertEqual(response_content.get('notes'), '')

    def test_create_company_with_layoffs_should_succeed(self) -> None:
        response = self.client.post(self.companies_url, {'name': 'Kek Company', 'status': 'Layoffs'})
        #raise ValueError(response.content)
        self.assertEqual(response.status_code, 201)
        response_content = json.loads(response.content)
        self.assertEqual(response_content.get('name'), 'Kek Company')
        self.assertEqual(response_content.get('status'), 'Layoffs')

    def test_create_company_with_wrong_status_should_fail(self) -> None:
        response = self.client.post(self.companies_url, {'name': 'Kek Company', 'status': 'wrong status'})
        self.assertEqual(response.status_code, 400)
        #assertIn kak operator in
        #self.assertIn('wrong status', str(response.content))
        self.assertIn('is not a valid choice.', str(response.content))
        print(response.content)

    #этот декоратор заставляет пайтест не засчитывать тест как фейл, если он фейлит, а заносит его в отдельную категорию xfailed
    @pytest.mark.xfail
    def test_should_be_ok_if_fail(self) -> None:
        self.assertEqual(1,2)


    #этот декоратор заносит ntcn в отдельную категорию skipped, ну и собсна скипает его
    @pytest.mark.skip
    def test_should_be_skipped(self) -> None:
        self.assertEqual(1,2)

    
#таааак, чтобы прогнать django тест с помощью pytest, пришлось подзапариться, нужна была библиотека pytest-django
#в итоге py manage.py test не прошел, т.к. датабаза уже заполнена и возвращается не пустой лист
#а с пайтестом тест прошел, т.к. пайтест разворачивает на момент теста свою датабазу с той же схемой и потом удаляет ее? как-то так
#и это вроде нойс подход, т.к. не советуют миксовать продакшн датабазу с тестовой
