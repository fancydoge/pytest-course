from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from .serializers import *
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core.mail import send_mail

# Create your views here.
class CompanyViewSet(viewsets.ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all().order_by('-last_update')
    paginatios_class = PageNumberPagination


#этот декоратор заставляет расценивать ф-цию как post endpoint
@api_view(http_method_names=['POST'])
def send_company_email(request):

    send_mail(
    subject = request.data.get('subject'),
    message = request.data.get('message'),
    from_email = 'djnagotestmailkek@gmail.com',
    recipient_list = ['djnagotestmailkek@gmail.com'],
    )
    
    return Response({'status': 'success', 'info': 'email sent correctly'}, status=200)