from unittest import TestCase
from django.test import Client
from django.urls import reverse
import json
#unittest - стандартный тест фреймворк, включенный в python
#все что можно сделать с помощью unittest, можно сделать и с помощью PyTest, но гораздо быстрее и проще и чище


#Самый простой кейс - есть ли компании в датабазе вообще?
class TestGetCompanies(TestCase):
    #префикс test_, чтобы pytest мог его видеть
    #zero_companies - чать, которая говорит о state(случае,видимо), который рассматривается
    #ну и остальная часть - ожидаемый результат
    # -> None - это просто примечание о том, что должна возвращать ф-ция, а так она может вернуть что угодно
    def test_zero_companies_should_return_empty_list(self)->None:
        #берем тестовый клиент django^, который работает как тот же Postman
        client = Client()
        #url, куда будут посылаться запросы, но хардкодить их - плохая практика
        #companies_url = "http://127.0.0.1:8000/companies/"
        companies_url = reverse('companies-list')
        response = client.get(companies_url)
        #неудобный синтаксис django тестов
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), [])