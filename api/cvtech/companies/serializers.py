from rest_framework import serializers
from .models import *

#преобразует модель джанго в JSON формат
class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name', 'status', 'last_update', 'application_link', 'notes']