from rest_framework import routers

from .views import CompanyViewSet

#routers.DefaultRouter ведет в Root Api со всеми меющимися url
companies_router = routers.DefaultRouter()
#basename - упрощенный доступ к этомй url, как name в path
#первый аргумент - это адрес /
companies_router.register('companies', viewset=CompanyViewSet, basename='companies')