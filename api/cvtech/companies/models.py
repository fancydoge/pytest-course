from django.db import models
from django.utils import timezone

# Create your models here.
class Company(models.Model):

    class Meta:
        verbose_name_plural = "Companies"

    class ComanyStatus(models.TextChoices):
        LAYOFFS = 'Layoffs',
        HIRING_FREEZE = 'Hiring Freeze',
        HIRING = 'Hiring',

    name = models.CharField(max_length=50, null=False, blank=False, unique=True)
    status = models.CharField(max_length=50, choices=ComanyStatus.choices, default=ComanyStatus.HIRING)
    #As currently implemented, setting auto_now or auto_now_add to True will cause the field to have editable=False and blank=True set.
    #поэтому, чтобы иметь возможность изменить таймстэмп вручную, нужно юзать default vmesto auto_now_add и editable = True
    last_update = models.DateTimeField(default=timezone.now, editable=True)
    application_link = models.URLField(blank=True)
    notes = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.name